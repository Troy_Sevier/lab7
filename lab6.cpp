/**************************
 *Troy Sevier
 *CPSC 1021, 004 F20
 *tsevier@clemson.edu
 *Nushrat Humaira
 **************************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;


typedef struct Employee
{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
} employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[])
{

  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
  Employee employees[10];

  for(int i = 0; i <= 9; i++)
  {//user input and printing start menu
    cout <<"Enter employee "<< i +1 <<"'s First Name: "<< endl;
      cin >> employees[i].firstName;
    cout <<"Enter employee "<< i + 1 <<"'s Last Name: "<< endl;
      cin >> employees[i].lastName;
    cout <<"Enter employee "<< i + 1 <<"'s birth year: "<< endl;
      cin >> employees[i].birthYear;
    cout <<"Enter employee "<< i + 1 <<"'s hourly wage: "<< endl;
      cin >> employees[i].hourlyWage;
			cout << endl;
  }
	cout << endl;

  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

	random_shuffle(&employees[0], &employees[10], *myrandom); //the array doin the cupid shuffle

   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
    Employee smallArr[5];//new array

    for(int i = 0; i <=4; i++)
    {//for loop that sets values of the new array to the values of the first one
      smallArr[i] = employees[i];
    }

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
		 sort(&smallArr[0], &smallArr[10], name_order);//using the sort functioin

    /*Now print the array below */
		for(int i = 0; i <= 4; i++)
		{//loop that prints the array
			cout << setw(5) << smallArr[i].lastName << ", " << smallArr[i].firstName << endl;
			cout << setw(5) << smallArr[i].birthYear << endl;
			cout << setw(5) << smallArr[i].hourlyWage << endl;
			cout << endl;
		}

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs)
{//function that compares the last names in the array to determine alphabetical order
	if(lhs.lastName < rhs.lastName)
	{
		return true;
	}
	else return false;
	return 0;
}
